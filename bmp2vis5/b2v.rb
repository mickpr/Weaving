#!/usr/bin/ruby
#require 'rubygems'
require 'json'
require 'rmagick'
require './weaving'	# my local class module
#require 'pp'


puts "\nStart..."
start_time = Time.now

json = File.read('bitmap.json') #<-- todo: ---------------------------------------


# (1) Read configuration from JSON and make table with warps and wefts objects
wefts = []    # table of wefts
warps = []    # table of warps

begin
 	tags = JSON.parse(json)

 	bitmap_file = tags["BitmapFile"]

	i = 0 
 	tags["Wefts"].each do | item |
 		wefts[i] = Weaving::Weft.new(item["color"],
 									item["width"].to_i,
 									item["spacing"].to_i)
 		i = i+1
 	end

 	i=0
 	tags["Warps"].each do | item |
 		warps[i] = Weaving::Warp.new(item["color"],
 									item["width"].to_i,
 									item["spacing"].to_i)
 		i = i+1
 	end

 rescue Exception =>e
 	    puts e.message
 	    puts e.backtrace.inspect
 	    raise '--- Error parsing JSON pattern description file ---'
 	exit
end

# (2) Get pattern of fabric (black/white)
pattern = nil
begin
	pattern = Weaving::Pattern.new(bitmap_file)
rescue Exception =>e
	    puts e.message
	    puts e.backtrace.inspect
	    raise 'FileOpenError'
	exit
end


# (3) Store width and height of pattern in simple variables
pattern_width = pattern.width
pattern_height= pattern.height

# (4) Count width and height of destination image 
dest_width = 0
dest_height = 0

y=0 
while y< pattern_height
	dest_height = dest_height+(wefts[y%wefts.size]).spacing
	y=y+1
end

#dest_height = dest_height+8
for x in 0..(pattern_width-1)
		dest_width = dest_width + (warps[x%warps.size]).spacing
end

 
#puts dest_width
#puts dest_height

# (5) Create destination image 

img_dst = Magick::Image::new(dest_width,dest_height) { self.background_color = "Transparent" }
puts "Image dest size: #{img_dst.columns} x #{img_dst.rows} \n(#{pattern_width} warps x #{pattern_height} wefts ) "

exit

# (6) Create bricks table from given wefts/warps 
ricks = []
bricks = Weaving::Bricks.new(wefts,warps)
 
# bricks.display_all_bricks
# exit
#puts "brick 0"
#bricks.get_brick(1,1).display


# ......-------------------------------------------
#        OSNOWA = czarny, Wątek = biały
#

# pierwszy watek od gory zaczynamy jako ten na dole (drugi moze go wtedy przykryc)
#   czyli watki parzyste (0,2,4..n) rysujemy jako pierwsze tam, gdzie jest widoczna osnowa (watek pod spodem)
#   
#


# bricks.display_all_bricks
#exit
# (6) Iterare through pattern - najpierw rysujemy osnowy (V) nad wątkami
#
# [px, py] - point of pattern
px=0
py=0
# [dst_px, dst_py] - point in destination image 
dst_px=0
dst_py=0

# iterate through rows 
while py<pattern_height do
	# clear  px, dst_px
	px =0
	dst_px=0
	#iterate through columns 
	while px<pattern_width do
	 	# get color from pattern (b or w)
	 	color = pattern.pixel_color(px,py)
	 	# change it to RGB (8 bit)
	 	color_r = pattern.pixel_color(px,py).red/257 

	 	numer_watku = py % wefts.size    
	 	numer_osnowy= px % warps.size
		
	 	if color_r==0  
	 		# pobierz obrazek 
	 		brick_image = bricks.get_brick_warp(numer_watku,numer_osnowy)
	 		#brick_image.display
	 		brick_width = brick_image.columns  # width of brick
	 		brick_height= brick_image.rows	   # heidht (spacing)

	 		#pixels = brick_image.get_pixels(0,0,brick_width,brick_height)
	 		#img_dst.store_pixels(dst_px,dst_py, brick_width,brick_height,pixels)
			img_dst = img_dst.composite(brick_image,dst_px,dst_py,Magick::OverCompositeOp) 
	 	end
	 	dst_px=dst_px+(warps[px%warps.size]).spacing
	 	px=px+1
	 	#dst_px=dst_px-warps[px%warps.size]).spacing
	end 
	dst_py=dst_py + (wefts[py%wefts.size]).spacing
	#dst_py=dst_py + brick_height
	py=py+1
end


# bricks.display_all_bricks
# exit
# (6) Iterare through pattern - najpierw rysujemy osnowy (V) nad wątkami
#
# [px, py] - point of pattern
px=0
py=0
# [dst_px, dst_py] - point in destination image 
dst_px=0
dst_py=0

# # iterate through rows 
while py<pattern_height do
	# clear  px, dst_px
	px =0
	dst_px=0
	#iterate through columns 
	while px<pattern_width do
	 	# get color from pattern (b or w)
	 	color = pattern.pixel_color(px,py)
	 	# change it to RGB (8 bit)
	 	color_r = pattern.pixel_color(px,py).red/257 

	 	numer_watku = py % wefts.size    
	 	numer_osnowy= px % warps.size
		
	 	if color_r!=0  
	 		# pobierz obrazek 
	 		brick_image = bricks.get_brick_weft(numer_watku,numer_osnowy)
	 		#brick_image.display
	 		brick_width = brick_image.columns  # width of brick
	 		brick_height= brick_image.rows	   # heidht (spacing)

	 		#pixels = brick_image.get_pixels(0,0,brick_width,brick_height)
	 		#img_dst.store_pixels(dst_px,dst_py, brick_width,brick_height,pixels)
	 		img_dst = img_dst.composite(brick_image,dst_px,dst_py,Magick::OverCompositeOp) 
	 	end
	 	dst_px=dst_px+(warps[px%warps.size]).spacing
	 	px=px+1
	 	#dst_px=dst_px-((warps[px%warps.size]).width-(warps[px%warps.size]).spacing)
	end 
	dst_py=dst_py + (wefts[py%wefts.size]).spacing
	#dst_py=dst_py + brick_height
	py=py+1
end


img_dst.write("dest.png")

#pp color_tab
#pattern = nil
#	pattern = Weaving::Pattern.new("wzor_hearts_kolejne.png")
#
#color_tab.each do |item | 
#	puts item
#end

stop_time = Time.now
diff = stop_time - start_time
puts "#{diff} seconds"
