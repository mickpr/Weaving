module Weaving

include Magick
#require 'pp'

#-----------------------------------------------------------------------------------------
# klasa wzorca weft, pamietajaca obrazek, grubosc, odstep
class Weft
	def initialize(color,width,spacing,filename)
		@color = color
		@width = width
		@spacing = spacing
		@filename = filename
		begin 
		    @image = Magick::Image::read(@filename).first
			@image.background_color = "Transparent"
		rescue Exception => e
		    puts e.message
		    puts e.backtrace.inspect
		    raise '--- Reading Weft Image Error ---'
		end
	end

	attr_accessor :color, :width, :spacing, :filename, :image
end
#-----------------------------------------------------------------------------------------
# klasa wzorca warp, pamietajaca obrazek, grubosc, odstep
class Warp
	def initialize(color,width,spacing,filename)
		@color = color
		@width = width
		@spacing = spacing
		@filename = filename
		begin
			@image = Magick::Image::read(@filename).first
			@image.background_color = "Transparent"
		rescue Exception => e
		    puts e.message
		    puts e.backtrace.inspect
		    raise '--- Reading Warp Image Error ---'
		end
	end
	attr_accessor :color, :width, :spacing, :filename, :image
end

#--------------------------------------------------------------------------------------
# klasa patternu do iteracji po wzorze figury.
# Kolory wzoru i definicji muszą sie zgadzac co do bitu!
class Pattern
	def initialize(image)
		begin 
		    @image = Magick::Image::read(image).first
		rescue Exception => e
		    puts e.message
		    puts e.backtrace.inspect
		    raise 'FileOpenError'
		end
	end

	def to_s
		"Pattern image (#{@pattern.columns},#{@pattern.rows})"
	end

	def pixel_color(x,y)
		@image.pixel_color(x,y)
	end

    def width
		@image.columns
    end

    def height
		@image.rows
    end

	#pixel_color(px,py).red
	#They are stored in a 'quantum depth' of 16-bits. You can rebuild the library to change this. 
	#Or you can simply divide each value by 257.
	#There's a function called MagickExportImagePixels which can get you the 8-bit pixel data that you want. 
	#Whenever you perform a transformation etc on an image it will get converted back to 16-bit pixels.	#
	#def pixelcolor(x,y)
	#	puts @pattern.pixel_color(x,y).red/257
	#	puts @pattern.pixel_color(x,y).green/257
	#	puts @pattern.pixel_color(x,y).blue/257
	#end

    #attr_reader :color, :weft1, :weft2
	#attr_writer :color, :weft1, :weft2
	#attr_accessor :color, :weft1, :weft2

end # class Patter

#-------------------------------------------------------------
# klasa tworzaca i przechowujaca wzorce obrazkow do wstawiania bedace zlazeniem watku i osnowy
# (wszystkie kombinacje zlozonych watkow i osnow).
# 
# klasa po stworzeniu zwraca tablice obiektow bedaca zlozeniem wszystkich watkow i osnow \
# w kolejnosci  watek1->osnowa1, watek1->osnowa2, watek2->osnowa1, watek2->osnowa2
#

class Bricks

	def initialize(wefts, warps)

		# pobierz do zmiennej
		@wefts_count = wefts.size
		@warps_count = warps.size
			
		i=0
		@bricks = [wefts.size*warps.size*2]  # 2 because there are 2 cases (weft in front of warp, and opposite)

		wefts.each do | weft_item |
			warps.each do | warp_item | 

				# pobiez obrazki
				@img_weft = weft_item.image
				@img_warp = warp_item.image
						
				# rozszesz obrazki do pelnych wymiarow klocka
				# @img_weft=weft_item.image.resize(warp_item.spacing,weft_item.width)
				# @img_warp=warp_item.image.resize(warp_item.width,weft_item.spacing)


				# maksymala szerokosc obrazka (osnowy)
				if warp_item.spacing>warp_item.width 
					warp_max_width = warp_item.spacing
				else
					warp_max_width = warp_item.width
				end 

				# maksymalna wysokosc obrazka (watku)
				if weft_item.spacing>weft_item.width 
					weft_max_height = weft_item.spacing
				else
					weft_max_height = weft_item.width
				end

				# ale skladowe obrazka nie rozciagamy na maks

				# ..watek rozciagamy tylko w poziomie
				@img_weft=weft_item.image.resize(warp_max_width,weft_item.width)
				#.. osnowe rozciagamy w pionie
				@img_warp=warp_item.image.resize(warp_item.width,weft_max_height)

				@img_weft.background_color = "Transparent"
				@img_warp.background_color = "Transparent"

				# pokolorujmy
				@img_weft= @img_weft.colorize(0.7, 0.7, 0.7, 0, weft_item.color)
				@img_warp= @img_warp.colorize(0.7, 0.7, 0.7, 0, warp_item.color)
				# contrast
				
				@img_weft.contrast(0)
				@img_warp.contrast(0)
				
				# ------- tutaj img_weft i img_warp zawieraja obrazki wylacznie (bez odstepow,pustych miejsc)
				# teraz nalezy je nakladac na siebie tworzac wiekszy obrazek o szerokosci i wysokosci=spacing 

				#stworz puste obrazki dla tworzenia 'kafelka'
				img_weft_new= Magick::Image.new(warp_max_width,weft_max_height) { self.background_color = "Transparent" }
				img_warp_new= Magick::Image.new(warp_max_width,weft_max_height) { self.background_color = "Transparent" } 
			
				# img_weft_new= Magick::Image.new(warp_item.spacing,weft_item.width) { self.background_color = "Transparent" }
				# img_warp_new= Magick::Image.new(weft_item.spacing,warp_item.width) { self.background_color = "Transparent" } 

				# wpisz na nie weft i warp
				img_weft_new.composite!(@img_weft,Magick::CenterGravity,Magick::OverCompositeOp)
				img_warp_new.composite!(@img_warp,Magick::CenterGravity,Magick::OverCompositeOp)
				# ----- tutaj mamy obrazki z pustym miejscem, odstepami

				# teraz naloz (v na h) i (h na v)
				#img_weft_new = img_warp_new.composite(@img_weft,Magick::CenterGravity,Magick::OverCompositeOp)
				#img_warp_new = img_weft_new.composite(@img_warp,Magick::CenterGravity,Magick::OverCompositeOp) 

				@img_weft = img_weft_new
				@img_warp = img_warp_new

				@width = @img_weft.columns
				@height = @img_weft.rows

				#@img_weft.display
				#@img_warp.display
			 	
			 	@bricks[i] = @img_weft
				@bricks[i+1] =@img_warp
			 	i=i+2
			end
		end
	end


	# parametry podawane od 0.. warps/wefts_count(-1)
	# def get_brick(weft_number, warp_number)
	# 	if weft_number>=@wefts_count || warp_number>=@warps_count
	# 		raise "--- You put wrong no. of weft/warp while getting the brick ---"
	# 	end
	# 	@bricks[weft_number*@warps_count + warp_number]
	# end

	def get_brick_weft(weft_number,warp_number)
		warp_no = warp_number % @warps_count
		weft_no = weft_number % @wefts_count

		position = weft_no*@warps_count*2 + warp_no*2
		@bricks[position]
	end

	def get_brick_warp(weft_number,warp_number)
		warp_no = warp_number % @warps_count
		weft_no = weft_number % @wefts_count
		position = weft_no*@warps_count*2 + warp_no*2+1
		@bricks[position]
	end


	# def get_brick_warp(weft_number,warp_number)
	# 	warp_no = warp_number % @warps_count
	# 	weft_number
	# 	@bricks[warp_number%wet ft_number*2) % @wefts_count +1]
	# end


	def display_all_bricks
		@bricks.each do | item | 
			item.display
		end
	end


end 

#-----------------------------------------------------------------------------------------
end #module1