require 'rmagick'
require './weaving'	# my local class module

weaves_dir = "./weaves/"	# wzory splotow

puts "\nStart..."
start_time = Time.now

# (1) Tworzymy zmienna reprezentujaca wzor tkaniny
pattern = nil
begin
	pattern = Weaving::Pattern.new("wzor_hearts_kolejne.png")
rescue Exception =>e
	    puts e.message
	    puts e.backtrace.inspect
	    raise 'FileOpenError'
	exit
end
# (2) Tworzymy zmienne pamietajace poszczegolne wzory (sploty)
weaves=	[]
weaves << Weaving::Weave.new(weaves_dir + 'hearts20x20_satyna.png')
weaves << Weaving::Weave.new(weaves_dir + 'hearts20x20_atlas.png')

# (3) Laczymy poszczegolne kolory ze splotami w hash table
color_tab = {}
color_tab['#fe0000'] = weaves[0]
color_tab['#ffffff'] = weaves[1]

color_tab2 = {}
color_tab2['#fe0000'] = weaves[1]  # inwersje wzorow
color_tab2['#ffffff'] = weaves[0] 

#color_tab.keys.each do |x| 
	#puts x
#	puts color_tab[x].width.to_s + " x " + color_tab[x].height.to_s
#end

# (4) Stworz reprezentacje pikselowa obrazow watkow i osnow
#     Ich rozmiary i kolory moga być różne
wefts = []
warps = []

# dodajmy dwa watki, o kolorze, grubosci i wzorcu 
wefts << Weaving::Weft.new('#e00000',4,'h4.png') #grubszy watek
wefts << Weaving::Weft.new('#ffffff',4,'h4.png') #cienszy watek
# tak samo dodajmy jedną osnowę
warps << Weaving::Warp.new('#ffffff',4,'v4.png')  #osnowa
# zefiniujmy odleglosc miedzy srodkami watkow
wefts_spacing =6  	# odleglosc miedzy watkami (kolejnymi parzystymi lub nieparzystymi) 
					# (0 oznacza rysowanie watkow przylegajacych do siebie
					# wartość >0 oznacza rozsuniecie miedzy watkami )
# ... i osnów
warps_spacing =4   # odleglosc miedzy osnowami (kolejnymi)

# wykorzystywane dla tkaniny dwuwatkowej... 
#
wefts_between = 2   # odstep miedzy watkami parzystymi i nieparzystymi 
					# (0 oznacza, ze watek wyzej przykrywa ten biegnacy nizej
					# wartosc >0 oznacza rozsuniecie). 
					#0					# Maksymalne wefts_between = wefts_spacing/2  1
# (5) tabela wzorcow splotow dwoch nici dla kazdej osnowy i kazdego watku
bricks =[wefts.size*warps.size]

# uzupelnij tabele wzorcami.
b_index=0
i=0
while i<warps.size
	j=0
	while j<wefts.size
		bricks[i+j*(wefts.size-1)] = Weaving::Brick.new(wefts[j].thickness, warps[i].thickness, \
										 			wefts_spacing, warps_spacing,  \
										 			wefts[j].filename, warps[i].filename, \
										 			wefts[j].color, warps[i].color)
		j=j+1
	end
	i=i+1
end
	
# bricks.each do |x| 
#  x.img_weft.display
#  x.img_warp.display
#  puts x.height.to_s
#  puts x.width.to_s
# end

# exit

# (6) obliczanie wymiaru docelowego plotna
dest_height=0
dest_width=0

# TODO: sprawdz ponizej, czy to dobrze oblicza 

# szerokosc plotna
# na poczatek mamy polowe grubosci osnowy [0]
dest_width = dest_width+ (warps[0].thickness/2)
# teraz dla kazdej osnowy procz ostatniej mam1 warps_spacing
dest_width = dest_width + (warps_spacing * (pattern.width-1))
# i na koncu mamy polowe grubosci ostatniego watka
dest_width = dest_width + (warps[(pattern.width-1)% warps.size].thickness/2)
# wysokosc (dlugosc) plotna
# na poczatku mamy polowe grubosci watka[0] 
dest_height = dest_height + (wefts[0].thickness/2)
# teraz dla kazdego watka (procz ostatniego)1dodajemy wefts_spacing
dest_height = dest_height + (wefts_spacing * (pattern.height-1))
# dodaj polowe grubosci ostatniego watka
dest_height = dest_height + (wefts[(pattern.height-1) % wefts.size].thickness/2)
# dodaj dodatkowo odleglosc miedzy watka1i parzystymi (jesli watkow jest >1)
if wefts.size>1
	dest_height = dest_height + wefts_between  
end


#puts "#{dest_width} x #{dest_height}"
#exit

# (6) tworz wynikowy obrazek o podanych parametrach
img_dst = Magick::Image::new(dest_width+100,dest_height+100) { self.background_color = "Transparent" }
puts "Image dest size: #{img_dst.columns} x #{img_dst.rows}"

# rozmiar wzorca, po ktorym bedziemy "jechac" podczas tworzenia obrazka
pattern_width = pattern.width
pattern_height = pattern.height

#zmienna dodatkowa do biezacej pozycji wstawianego klocka
puts "Wzorców w bricks:" + bricks.size.to_s

# px i py to punkty wzorca (pattern)
px=0
py=0
#dst_px, dst_py to punkty w obrazie wynikowym (pamietamy gdzie wkleic nastepny klocek)
dst_px=0
dst_py=0
#zaczynamy w pionie od grub0sci watka/2 (tak aby narysowany watek sie zmiescil od poczatku ekranu)
#dst_py= wefts[0].thickness/2

# warunek - pattern musi miec parzystą ilość wierszy (watkow)

#petla wierszami (tak jak biegna kolejne watki) 
while py<pattern_height do
	# dla kazdego watka na poczatku zeruj px i dst_px
	px =0
	dst_px=0
	# teraz petla kolumnami (tak jak biegną nitki osnowy)
	while px<pattern_width do

		#puts "#{px} x #{py}"

		#ustal numer kostek
		numer_osnowy = px % warps.size
		numer_w1 = py % wefts.size    # watek w1
		numer_w2 = (py+1) % wefts.size  # nastepny wiersz (watek w2)
		#numer_w1=0
		#numer_w2=1
		
		#puts "#{py} -> #{numer_w2}"  
		#

		# rozmiar watkow wg rozkladu
		w1_dx = bricks[(numer_w1*wefts.size-1) + numer_osnowy].width
		w1_dy = bricks[(numer_w1*wefts.size-1) + numer_osnowy].height

		w2_dx = bricks[(numer_w2*wefts.size-1) + numer_osnowy].width
		w2_dy = bricks[(numer_w2*wefts.size-1) + numer_osnowy].height
		

		#puts "#{w1_dx} x #{w1_dy} i  #{w2_dx} x #{w2_dy}"

		# # odczytaj kolor ze wzorca
		color = pattern.pixel_color(px,py)
		#zmien na postac RGB (8 bit)
		color_r = pattern.pixel_color(px,py).red/257
		color_g = pattern.pixel_color(px,py).green/257
		color_b = pattern.pixel_color(px,py).blue/257
		# zmien na wartosc HEX
		key = "#%02x%02x%02x" % [color_r,color_g,color_b]
		#puts "Key is #{key}"

		if color_tab[key]
			# is_black == true oznacza watek nad osnowa 
			w1_isblack = color_tab[key].is_black(px % color_tab[key].width, py % color_tab[key].height)
			w2_isblack = color_tab2[key].is_black(px % color_tab2[key].width, py % color_tab2[key].height)
			#puts "#{w1_isblack} #{w2_isblack}"

			# pamietamy, ze dst_py jest pozycja w srodku watka W1, dst_px jest od początku  
			# 
			# +----+
			# |    | 
			# +----+<---- srodek klocka (watku) = dst_py
			# |    |			# +----+
			# ^--- = dst_px
			#
			# watek w2 bedzie nizej o wefts_between

			# puts numer_w1
			# puts numer_w2
			# exit

			# puts numer_w1*wefts.size+numer_osnowy
			# puts numer_w2*wefts.size+numer_osnowy
			# exit

			pixels_w1_warp = bricks[numer_w1*wefts.size-1 + numer_osnowy].img_warp.get_pixels(0,0,w1_dx,w1_dy)
			pixels_w1_weft = bricks[numer_w1*wefts.size-1 + numer_osnowy].img_weft.get_pixels(0,0,w1_dx,w1_dy)

			pixels_w2_warp = bricks[numer_w2*wefts.size-2 + numer_osnowy].img_warp.get_pixels(0,0,w2_dx,w2_dy)
			pixels_w2_weft = bricks[numer_w2*wefts.size-2 + numer_osnowy].img_weft.get_pixels(0,0,w2_dx,w2_dy)



			#img_dst.store_pixels(0,0,w1_dx,w1_dy,pixels_w1_warp)
			#img_dst.display
			#puts bricks.size
			#bricks[1].img_weft.display
			#exit

			#puts pixels_w1_weft.methods
						
			#puts "#{dst_px} x #{dst_py} - #{w1_dx} x #{w1_dy}"

			if w1_isblack && w2_isblack
				#w1 i w2 obok siebie (rowne) 
				#printf("=")				
				img_dst.store_pixels(dst_px,dst_py, w1_dx, w1_dy, pixels_w1_weft)
				img_dst.store_pixels(dst_px,dst_py + wefts_between, w2_dx, w2_dy, pixels_w2_weft)
			end
			if w1_isblack && !w2_isblack
				#w1 nad w2
				#printf("-")				
				img_dst.store_pixels(dst_px,dst_py+ wefts_between, w2_dx, w2_dy, pixels_w2_warp)
				img_dst.store_pixels(dst_px,dst_py, w1_dx, w1_dy, pixels_w1_weft)
			end
			if !w1_isblack && w2_isblack
				#w2 nad w1
				#printf("~") 
				img_dst.store_pixels(dst_px,dst_py, w1_dx, w1_dy, pixels_w1_warp)
				img_dst.store_pixels(dst_px,dst_py + wefts_between, w2_dx, w2_dy, pixels_w2_weft)
			end
			if !w1_isblack && !w2_isblack
				#w1 pod w2 pod osnowa obydwa (osnowa)
				#printf("|")
				img_dst.store_pixels(dst_px,dst_py, w1_dx, w1_dy, pixels_w1_warp)
				img_dst.store_pixels(dst_px,dst_py + wefts_between, w2_dx, w2_dy, pixels_w2_warp)
			end

		else
			puts "Color #{key} doesn't exist in color table"
		end

		# 	#jesli podany kolor istnieje w zdefiniowanej tabeli kolorow(kolor/wzorzec splotu)
		# 	if color_tab[key]
		# 		# najpierw zakryty przez osnowe watek
		# 		if (!color_tab[key].is_black(px % color_tab[key].width, py % color_tab[key].height))
		# 	 		#pixels_source = bricks[numer_watku*wefts.size + numer_osnowy].img_warp.get_pixels(0,0,dst_dx,dst_dy) 
		# 			#img_dst.store_pixels(dst_px,dst_py,dst_dx,dst_dy,pixels_source)
		# 		end
		# 	end

		# #puts "#{px} x #{py}"

		dst_px=dst_px + warps_spacing
	 	px=px+1

	end 
	#printf("\n")

	# #
	# dst_py=dst_py + wefts_spacing
	py=py+1
	dst_py=dst_py + wefts_spacing
	
end


#img_dst=img_dst.contrast(true)
#img_dst=img_dst.contrast(true)
# img_dst=img_dst.contrast(true)
#img_dst = img_dst.blur_image(3,5)
img_dst.write("bitmap.png")

stop_time = Time.now
diff = stop_time - start_time
puts "#{diff} seconds"

