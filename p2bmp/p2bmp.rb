#!/usr/bin/ruby
#require 'rubygems'
require 'json'
require 'rmagick'
require './weaving'	# my local class module
#require 'pp'

weaves_dir = "./weaves/"	# weaves 

puts "\nStart..."
start_time = Time.now

json = File.read('heart2.json') #<-- todo: ---------------------------------------

# (1) Make has table with colours points to weaves object 

weaves = []    # table of weaves objects (in memory)
color_tab = {} # association (hash) table of colors points to weaves (objects)

begin
	tags = JSON.parse(json)

	tags["Weaves"].each do | item |
		weaves << Weaving::Weave.new(weaves_dir + item["weave"]) # add object to array table
		color_tab[item["color"]] = weaves[weaves.size-1]
		#puts item["color"] + " - " + item["weave"]
	end
rescue Exception =>e
	    puts e.message
	    puts e.backtrace.inspect
	    raise 'Error parsing JSON pattern description file'
	exit
end

# (2) Create pattern of the fabric (pattern of material)
pattern = nil
begin
	pattern = Weaving::Pattern.new("heart2.png")
rescue Exception =>e
	    puts e.message
	    puts e.backtrace.inspect
	    raise 'FileOpenError'
	exit
end

# (4) Store width and height of pattern in simple variables
dest_width = pattern.width
dest_height= pattern.height

pattern_width = pattern.width
pattern_height= pattern.height

# (5) Create destination image 
img_dst = Magick::Image::new(dest_width,dest_height) { self.background_color = "Transparent" }
puts "Image dest size: #{img_dst.columns} x #{img_dst.rows}"


# (6) Iterare through pattern, 
#     get pixel color 
#     select appropriate weave according color/weave pair defined in json definition file, 
#     put black/white point from weave pattern (using modulo) into dest image    
#
# [px, py] - point of pattern
px=0
py=0
# [dst_px, dst_py] - point in destination image 
dst_px=0
dst_py=0

# iterate through rows 
while py<pattern_height do
	# clear  px, dst_px
	px =0
	dst_px=0
	# iterate through columns 
	while px<pattern_width do

		#puts "#{px} x #{py}"

		# get color from pattern
		color = pattern.pixel_color(px,py)
		# change it to RGB (8 bit)
		color_r = pattern.pixel_color(px,py).red/257
		color_g = pattern.pixel_color(px,py).green/257
		color_b = pattern.pixel_color(px,py).blue/257
		# change it to HEX string
		key = "#%02x%02x%02x" % [color_r,color_g,color_b]
		#puts "Key is #{key}"

		if color_tab[key]
			isblack = color_tab[key].is_black(px % color_tab[key].width, py % color_tab[key].height)
			if (isblack)
				img_dst.pixel_color(dst_px,dst_py, "#000000")
			else
				img_dst.pixel_color(dst_px,dst_py, "#FFFFFF")
			end
		else
			puts "Color #{key} doesn't exist in color table"
			exit
		end
		dst_px=dst_px+1
	 	px=px+1
	end 
	#printf("\n")
	py=py+1
	dst_py=dst_py + 1
	
end
img_dst.write("dest.png")



#pp color_tab
#pattern = nil
#	pattern = Weaving::Pattern.new("wzor_hearts_kolejne.png")
#
#color_tab.each do |item | 
#	puts item
#end

stop_time = Time.now
diff = stop_time - start_time
puts "#{diff} seconds"
