module Weaving

include Magick

MAX_MATERIAL_WIDTH =5110

# Klasa sciegu (weave) oferuje podstawowa funkcjonalnosc pobierania wzoru sciegu (0/1) 
# z obrazka sciegu -> mapa bitowa jednobitowa (000000/FFFFFF).  
# (czarny/true) oznacza watek (weft), (bialy/false) oznacza osnowe (warp)
class Weave 
    def initialize(image)
    	@color = ''
		@current_row = 0
		@filename = image
		begin 
		    @image = Magick::Image::read(@filename).first
		rescue Exception => e
		    puts e.message
		    puts e.backtrace.inspect
		    raise 'FileOpenError'
		end
	end

    # make methods private
    #private :getWidth, :getHeight

    public def to_s
		"Weave: #{@filename}, #{@current_row}"  
    end
    
    attr_accessor :color, :current_row, :filename

    def is_black(x,y)
		if ((@image.pixel_color(x,y).red==0) && (@image.pixel_color(x,y).green==0) && (@image.pixel_color(x,y).blue==0))
		    true
		else
		    false
		end
    end
    
    def width
		@image.columns
    end

    def height
		@image.rows
    end

end # class Weave

#--------------------------------------------------------------------------------------
# klasa patternu do iteracji po wzorze figury.
# Kolory wzoru i definicji muszą sie zgadzac co do bitu!
class Pattern
	def initialize(image)
		begin 
		    @image = Magick::Image::read(image).first
		rescue Exception => e
		    puts e.message
		    puts e.backtrace.inspect
		    raise 'FileOpenError'
		end
	end

	def to_s
		"Pattern image (#{@pattern.columns},#{@pattern.rows})"
	end

	def pixel_color(x,y)
		@image.pixel_color(x,y)
	end

    def width
		@image.columns
    end

    def height
		@image.rows
    end

	#pixel_color(px,py).red
	#They are stored in a 'quantum depth' of 16-bits. You can rebuild the library to change this. 
	#Or you can simply divide each value by 257.
	#There's a function called MagickExportImagePixels which can get you the 8-bit pixel data that you want. 
	#Whenever you perform a transformation etc on an image it will get converted back to 16-bit pixels.
	#
	#def pixelcolor(x,y)
	#	puts @pattern.pixel_color(x,y).red/257
	#	puts @pattern.pixel_color(x,y).green/257
	#	puts @pattern.pixel_color(x,y).blue/257
	#end

    #attr_reader :color, :weft1, :weft2
	#attr_writer :color, :weft1, :weft2
	#attr_accessor :color, :weft1, :weft2

end # class Patter
#-----------------------------------------------------------------------------------------
end #module1