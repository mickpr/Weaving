module Weaving
include Magick

MAX_MATERIAL_WIDTH =5110

# Klasa sciegu (weave) oferuje podstawowa funkcjonalnosc pobierania wzoru sciegu (0/1) 
# z obrazka sciegu -> mapa bitowa jednobitowa (000000/FFFFFF).  
# (czarny/true) oznacza watek (weft), (bialy/false) oznacza osnowe (warp)
class Weave 
    def initialize(image)
    	@color = ''
		@current_row = 0
		@filename = image
		begin 
		    @image = Magick::Image::read(@filename).first
		rescue Exception => e
		    puts e.message
		    puts e.backtrace.inspect
		    raise 'FileOpenError'
		end
	end

    # make methods private
    #private :getWidth, :getHeight

    public def to_s
		"Weave: #{@filename}, #{@current_row}"  
    end
    
    attr_accessor :color, :current_row, :filename

    def is_black(x,y)
		if ((@image.pixel_color(x,y).red==0) && (@image.pixel_color(x,y).green==0) && (@image.pixel_color(x,y).blue==0))
		    true
		else
		    false
		end
    end

    def width
		@image.columns
    end

    def height
		@image.rows
    end

end # class Weave

#--------------------------------------------------------------------------------------
# klasa patternu do iteracji po wzorze figury.
# Kolory wzoru i definicji muszą sie zgadzac co do bitu!
class Pattern
	def initialize(image)
		begin 
		    @image = Magick::Image::read(image).first
		rescue Exception => e
		    puts e.message
		    puts e.backtrace.inspect
		    raise 'FileOpenError'
		end
	end

	def to_s
		"Pattern image (#{@pattern.columns},#{@pattern.rows})"
	end

	def pixel_color(x,y)
		@image.pixel_color(x,y)
	end

    def width
		@image.columns
    end

    def height
		@image.rows
    end

	#pixel_color(px,py).red
	#They are stored in a 'quantum depth' of 16-bits. You can rebuild the library to change this. 
	#Or you can simply divide each value by 257.
	#There's a function called MagickExportImagePixels which can get you the 8-bit pixel data that you want. 
	#Whenever you perform a transformation etc on an image it will get converted back to 16-bit pixels.
	#
	#def pixelcolor(x,y)
	#	puts @pattern.pixel_color(x,y).red/257
	#	puts @pattern.pixel_color(x,y).green/257
	#	puts @pattern.pixel_color(x,y).blue/257
	#end

    #attr_reader :color, :weft1, :weft2
	#attr_writer :color, :weft1, :weft2
	#attr_accessor :color, :weft1, :weft2

end # class Patter
#-----------------------------------------------------------------------------------------
# klasa wzorca weft, pamietajaca obrazek, grubosc, odstep
class Weft
	def initialize(color,thickness,filename)
		@color = color
		@thickness = thickness
		@filename = filename
	end
	attr_accessor :color, :thickness, :filename
end
#-----------------------------------------------------------------------------------------
# klasa wzorca warp, pamietajaca obrazek, grubosc, odstep
class Warp
	def initialize(color,thickness,filename)
		@color = color
		@thickness = thickness
		@filename = filename
	end
	attr_accessor :color, :thickness, :filename
end

# klasa pamietajaca dwa obrazki bedace zlozeniem watka i osnowy
# generuje je na podstawie rozmiarow podanej kratki
class Brick
	def initialize(weft_thickness, warp_thickness, weft_spacing, warp_spacing, weft_filename, warp_filename, weft_color, warp_color)
		@weft_thickness = weft_thickness
		@warp_thickness = warp_thickness
		@weft_spacing = weft_spacing
		@warp_spacing = warp_spacing
		@weft_color = weft_color
		@warp_color = warp_color

		#@pixels_weft = nil
		#@pixels_warp = nil

		@img_weft = nil
		@img_warp = nil

		begin
		# read source images 
			@img_weft=Magick::Image::read(weft_filename).first  
			@img_warp=Magick::Image::read(warp_filename).first 
		rescue
			puts e.message
		    puts e.backtrace.inspect
		    raise 'FileOpenError'
		end	

		## control proper values
		if (weft_thickness>weft_spacing || warp_thickness>warp_spacing)
			raise 'ThicknessBiggerThanSpacing'
		end

		# rozszesz obrazki do pelnych wymiarow
		@img_weft=@img_weft.resize(warp_spacing,weft_thickness)
		@img_warp=@img_warp.resize(warp_thickness,weft_spacing)
		@img_weft.background_color = "Transparent"
		@img_warp.background_color = "Transparent"
		
		# @img_warp = @img_warp.contrast(true)
		# @img_weft = @img_weft.contrast(true)
		# @img_warp = @img_warp.contrast(true)
		# @img_weft = @img_weft.contrast(true)
		
		#pokolorujmy 
		@img_weft= @img_weft.colorize(0.9, 0.9, 0.9, 0, weft_color)
		@img_warp= @img_warp.colorize(0.9, 0.9, 0.9, 0, warp_color)
		
		# ------- tutaj img weft i img_warp zawieraja obrazki wylacznie (bez odstepow) 
 
		#stworz puste obrazki dla tworzenia 'kafelka'
		img_weft_new= Magick::Image.new(warp_spacing,weft_spacing) { self.background_color = "Transparent" }
		img_warp_new= Magick::Image.new(warp_spacing,weft_spacing) { self.background_color = "Transparent" } 

		# wpisz na nie weft i warp
		img_weft_new.composite!(@img_weft,Magick::CenterGravity,Magick::OverCompositeOp)
		img_warp_new.composite!(@img_warp,Magick::CenterGravity,Magick::OverCompositeOp)

#CopyOpacity

		#stworz gradient
		#mask_gr_v = Magick::GradientFill.new(0,weft_spacing/2, warp_spacing,weft_spacing/2, 'rgb(30%, 30%, 30%)','rgb(90%, 90%, 90%)')
		#mask_gr_h = Magick::GradientFill.new(warp_spacing/2,0, warp_spacing/2,weft_spacing, 'rgb(90%, 90%, 90%)','rgb(30%, 30%, 30%)')

			# mask_gr_v = Magick::Image.read("gradient:rgba(0,0,0,0.3)=rgba(0,0,0,0.7)") do
			#    self.size = "100x100"
			#
			#end
		
		# gradient na obrazek
		#img_weft_new = Magick::Image.new(warp_spacing, weft_spacing, mask_gr_h)
		#img_warp_new = Magick::Image.new(warp_spacing, weft_spacing, mask_gr_v)
		
		#img_weft_new.display
		#img_warp_new.display
		
		# teraz naloz (v na h) i (h na v)
		img_weft_new = img_warp_new.composite(@img_weft,Magick::CenterGravity,Magick::OverCompositeOp)
		img_warp_new = img_weft_new.composite(@img_warp,Magick::CenterGravity,Magick::OverCompositeOp) 

		@img_weft = img_weft_new
		@img_warp = img_warp_new

		@width = @img_weft.columns
		@height = @img_weft.rows

		#img_weft.display
		#skopiuj obrazki pikselowo do pozniejszego wstawiania
		#img_weft = img_weft.get_pixels(0,0,img_weft.columns,img_weft.rows) 
		#img_warp = img_warp.get_pixels(0,0,img_warp.columns,img_warp.rows) 

	end

	def to_s
		"Weft=> thickness: #{@weft_thickness}, spacing: #{@weft_spacing}, color: #{@weft_color} \n" + \
		"Warp=> thickness: #{@warp_thickness}, spacing: #{@warp_spacing},  color: #{@warp_color}"
	end
	
	attr_reader :img_weft, :img_warp, :width, :height

end 
#-----------------------------------------------------------------------------------------
end #module1